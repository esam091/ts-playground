import _ from 'lodash'
import t from 'io-ts'

const m = _.min([10, 20, 30])

const hello = "hello"

let a: string = "lalala"
a = "lilili"
a = 1
a = true

let b: number = 123
let bener: boolean = true

// null
// undefined



null == undefined // true

null === undefined // false

null === null // true

interface Human {
  name: string;
  age: number;
}

function printName(h: Human): void {
  console.log(h.name)
}

function countLength(s: string): number {
  return s.length
}

printName({
  name: "bondan",
  age: 47,
})

function getStringLength(name: string | null) {
  if (name) {
    return name.length
  }

  return 0
}

// union types
function setWidth(width: number | string | "lalala") {
  if (typeof width === 'string') {
    width.toUpperCase()

  } 
}

let kuda: any = "lalala"

function isHuman(foo: any): foo is Human {
  return foo.name && foo.zxcvzxcv
}

if (isHuman(kuda)) {
  
}


// type system
// swift/java: nominal typing
// typescript: structural typing

/* 
 
class Orang {
  func sayHello() {}
}

class Kuda {
  func sayHello() {}
}
  
let o = Orang()

let k: Kuda = o as! Kuda

*/

type TextAlignment = 'left' | 'right' | 'center' | 'justify' | 123

function setTextAlignment(alignment: TextAlignment) {
  //
}

setTextAlignment(123)

//discriminated union
type NetworkState<T> = { status: 'not-started' }
  | { status: 'loading' }
  | { status: 'loaded', data: T }
  | { status: 'error', error: string }

function setNetworkState(state: NetworkState<Human>) {
  switch(state.status) {
    case 'not-started': break;
    case 'loading': break;
    case 'loaded': console.log(state.data); break;
    case 'error': console.log(state.error); break;
  }
}

// intersection type
interface Hewan {
  tail: number
}

type Siluman = Human & Hewan

let campur: Siluman = {} as Siluman
campur.name.length

interface Product {
  id: string;
  name: string;
  variant: {
    color: string;
    size: number
  };
  status?: string
}

let p: Product = {} as Product
// p['name']

// keyof
function getProp<T, K extends keyof T>(obj: T, key: K): T[K] {
  return obj[key]
}

// getProp(p, 'names')

let p2: Product = {
  id: 'lala',
  name: 'sepatu',
  variant: {
    color: 'blue',
    size: 23
  },
}

function printStatus(p: Product) {
  if (p.status) {
    return p.status.toUpperCase()
  }

  return null
}

let text: string | null

function clearInput() {
  text = null
}

function printText() {
  
  
  if (text != null) {
    clearInput()
    console.log(text)
  }
}

function getDataFromServer(): string {
  return `{ foo: "foo", bar: "bar", epic: 123 }`
}

interface Foo {
  foo: string
  bar: string
  epic: string
}

const FooSchema = t.type({
  foo: t.string,
  bar: t.string,
  epic: t.string
})

const data = JSON.parse(getDataFromServer()) // any


if (FooSchema.is(data)) {
  
}