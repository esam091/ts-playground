import _ from 'lodash';
import t from 'io-ts';
const m = _.min([10, 20, 30]);
const hello = "hello";
let a = "lalala";
a = "lilili";
a = 1;
a = true;
let b = 123;
let bener = true;
// null
// undefined
null == undefined; // true
null === undefined; // false
null === null; // true
function printName(h) {
    console.log(h.name);
}
function countLength(s) {
    return s.length;
}
printName({
    name: "bondan",
    age: 47,
});
function getStringLength(name) {
    if (name) {
        return name.length;
    }
    return 0;
}
// union types
function setWidth(width) {
    if (typeof width === 'string') {
        width.toUpperCase();
    }
}
let kuda = "lalala";
function isHuman(foo) {
    return foo.name && foo.zxcvzxcv;
}
if (isHuman(kuda)) {
}
function setTextAlignment(alignment) {
    //
}
setTextAlignment(123);
function setNetworkState(state) {
    switch (state.status) {
        case 'not-started': break;
        case 'loading': break;
        case 'loaded':
            console.log(state.data);
            break;
        case 'error':
            console.log(state.error);
            break;
    }
}
let campur = {};
campur.name.length;
let p = {};
// p['name']
// keyof
function getProp(obj, key) {
    return obj[key];
}
// getProp(p, 'names')
let p2 = {
    id: 'lala',
    name: 'sepatu',
    variant: {
        color: 'blue',
        size: 23
    },
};
function printStatus(p) {
    if (p.status) {
        return p.status.toUpperCase();
    }
    return null;
}
let text;
function clearInput() {
    text = null;
}
function printText() {
    if (text != null) {
        clearInput();
        console.log(text);
    }
}
function getDataFromServer() {
    return `{ foo: "foo", bar: "bar", epic: 123 }`;
}
const FooSchema = t.type({
    foo: t.string,
    bar: t.string,
    epic: t.string
});
const data = JSON.parse(getDataFromServer()); // any
if (FooSchema.is(data)) {
}
